# container-run

An opinionated wrapper for running commands in containers. container-run
automates the mechanics of associating directories with container images
and working directories to run commands within those containers.

## How to use container-run

The `container-run` binary will attempt to find a container engine
(`podman` then `docker`) in the path, and then run any arguments after the
command (eg `container-run /bin/run --this=stuff --inside=container`) inside a
container that will have the current directory mounted inside with read/write
privileges.  In this manner a container environment can be used to read and
write files on the host filesystem, while a configuration file organizes
common container options.

### Install

Install from [crates.io][crates-page] using cargo.

1. `cargo install container-run`

### Create a configuration file

Create a file in one of three locations:
* `./.container-run.conf`
* `~/.container-run.conf`
* `~/.config/container-run/container-run.conf`

```json
{
    "basenames": {
        "myproject": {"profile": "golang-github"}
    },
    "profiles": {
        "golang-github": {
            "image": "docker.io/library/golang:1.16",
            "mountpoint": "/go/src/github.com/{basename}"
        }
    }
}
```

See the [container-run.conf documentation](/docs/config.md) for more detailed
information.

### Invoke container-run

Most frequently, container-run is used to execute software development oriented
commands within the scope of a container that has been constructed to provide
all the necessary build tools.

Change directory to the path you would like to execute commands within a
container, and execute the commands by prefixing them with `container-run`
(eg `$ container-run make test build`).

#### Example usage
With a working directory of `/home/me/myproject`, and the configuration
file from the configuration file section, running the
command `container-run make test` will do the following:
* determine the container engine to use (podman or docker)
* start running a container that:
    * uses the image `docker.io/library/golang:1.16`
    * mounts the host directory `/home/me/myproject/` into the container as `/go/src/github.com/myproject`
    * sets the working directory in the container to `/go/src/github.com/myproject`
    * executes the command `make test`
* passes execution to the container engine process, this will return all output and status
  to the shell from which it ran

[crates-page]: https://crates.io/crates/container-run

#### Enable debug logs

To enable debug log printing to the terminal, and a local `container-run.log`
file, set the environment variable `CONTAINER_RUN_VERBOSITY=debug` in your
shell before executing.
