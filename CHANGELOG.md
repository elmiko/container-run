# container-run Changelog

## 0.6.0

* add support for yaml configuration files, see [issue #10](https://gitlab.com/elmiko/container-run/-/issues/10)
* container namespace uses the same user id as parent process, see [issue #2](https://gitlab.com/elmiko/container-run/-/issues/10)

## 0.5.0

* add environment variable support, see [issue #4](https://gitlab.com/elmiko/container-run/-/issues/4)
* updated documentation
* internal refactor

## 0.4.1

* update simple_log config to not generate local files unless debug is set

## 0.4.0

* add docker runtime detection
* add engine to config file
* add some unit test and gitlab ci
