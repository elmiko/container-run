use anyhow::{bail, Result};
use simple_log::LogConfigBuilder;
use std::env;
use std::os::unix::process::CommandExt;
use std::process::Command;

#[macro_use]
extern crate log;

mod configuration;

const PODMAN: &str = "podman";
const DOCKER: &str = "docker";

fn main() -> Result<()> {
    let loglevel = get_log_level();
    let logconfig = match loglevel.as_str() {
        "debug" => LogConfigBuilder::builder()
            .path("./container-run.log")
            .level(loglevel)
            .output_file()
            .output_console()
            .build(),
        _ => LogConfigBuilder::builder()
            .level(loglevel)
            .output_console()
            .build(),
    };

    let _ = match simple_log::new(logconfig) {
        Ok(r) => Ok(r),
        Err(e) => bail(format!("unable to create simple_log config: {}", e)),
    };

    // target args are the command line argument passed to the container
    let mut target_args: Vec<String> = Vec::new();
    for a in env::args().skip(1) {
        target_args.push(a);
    }

    if target_args.is_empty() {
        bail("no commands passed to target container".to_string())?;
    }

    if target_args[0] == "--help" || target_args[0] == "-h" {
        usage();
        return Ok(());
    }

    if target_args[0].starts_with("-") {
        bail(format!(
            "first argument must be a command, looks like a flag: {}",
            target_args[0]
        ))?;
    }

    let path = env::current_dir().expect("cannot determine current dir");
    let basename = &path
        .file_name()
        .expect("cannot separate base path")
        .to_str()
        .unwrap();
    debug!("detected basename {:?}", &basename);

    let config = configuration::build_configuration(basename)?;
    debug!("config = {:?}", config);

    let engine = match config.engine.is_empty() {
        true => detect_engine()?,
        false => config.engine,
    };

    // selinux labeling does not work on macos yet, changing volume mount argument there for now
    let volume_mount_parameter = match cfg!(target_os = "macos") && engine == PODMAN {
        true => {
            warn!(
                "MacOS with podman detected, selinux labeling for volumes disabled. \
                See https://github.com/containers/podman/issues/13631#issuecomment-1077643246 \
                for more info"
            );
            format!("--volume={}:{}", path.to_str().unwrap(), &config.mountpoint)
        }
        false => format!(
            "--volume={}:{}:Z",
            path.to_str().unwrap(),
            &config.mountpoint
        ),
    };

    // container runtime args are the full list of arguments pass to the container runtime engine
    let mut container_engine_args = vec![
        "run".to_string(),
        "--rm".to_string(),
        "-it".to_string(),
        "--userns=keep-id".to_string(),
        volume_mount_parameter,
        format!("--workdir={}", config.mountpoint),
    ];

    for env in config.environment {
        let arg: String = if env.1.is_empty() {
            format!("--env={}", env.0)
        } else {
            format!("--env={}={}", env.0, env.1)
        };
        container_engine_args.push(arg);
    }

    // assemble the full arguments to the exec
    let mut full_args: Vec<String> = Vec::new();
    full_args.append(&mut container_engine_args);
    full_args.push(config.image);
    full_args.append(&mut target_args);

    // exec the container command and hand over execution
    debug!(
        "assembled exec command \"{} {}\"",
        &engine,
        full_args.join(" ")
    );
    Command::new(engine).args(full_args).exec();
    Ok(())
}

fn bail(message: String) -> Result<()> {
    usage();
    bail!(message);
}

fn detect_engine() -> Result<String> {
    // prefer podman
    for engine in vec![PODMAN, DOCKER] {
        let output = Command::new("which").arg(&engine).output()?;
        if output.status.success() {
            debug!("detected container engine \"{}\"", &engine);
            return Ok(engine.to_string());
        }
    }
    bail!("no container engine found")
}

fn get_log_level() -> String {
    let verbosity_key = "CONTAINER_RUN_VERBOSITY";
    let warn = String::from("warn");
    match env::var(verbosity_key) {
        Ok(val) => match val.as_str() {
            "debug" => String::from("debug"),
            _ => warn,
        },
        _ => warn,
    }
}

fn usage() {
    println!(
        "{}",
        format!("container-run version {}", env!("CARGO_PKG_VERSION"))
    );
    println!("Usage: container-run command_to_run_in_container arg1 arg2 --flag1 arg3");
}
