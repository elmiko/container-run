// Configuration file wrappers, see docs/config.md for more information
//
// The concept of the "basename" in this module refers to the key within
// the configuration file profiles that should be used. The main application
// uses the directory basename to associate with the container that the
// command should run within.
//
// Profiles are the specific information within the configuration file that
// define how the container should be run. There should also be a default
// profile within a well-formed configuration file. When transforming from a
// basename to a profile, default values will be substituted for any missing
// information.
use anyhow::{bail, Result};
use serde_yaml::Value;
use std::env;
use std::fs::File;
use std::io::BufReader;

// Info for running a container command.
#[derive(Debug)]
pub(crate) struct Configuration {
    pub image: String,
    pub mountpoint: String,
    pub engine: String,
    pub environment: Vec<(String, String)>,
}

// Return a Cofiguration given a basename key, or an error if one cannot be created.
pub(crate) fn build_configuration(basename: &str) -> Result<Configuration> {
    let filename = get_config_filename()?;
    let file = File::open(filename)?;
    let reader = BufReader::new(file);
    let v: Value = serde_yaml::from_reader(reader)?;

    let profile = profile_for_basename(&v, basename);
    debug!("profile = {:?}", &profile);
    let image = image_for_profile(&v, &profile)?;
    debug!("image = {:?}", &image);
    let mountpoint = mountpoint_for_profile(&v, basename, &profile)?;
    debug!("mountpoint = {:?}", &mountpoint);
    let engine = engine_for_profile(&v, &profile);
    debug!("engine = {:?}", &engine);

    let environment = environment_for_profile(&v, &profile);
    debug!("environment = {:?}", &environment);

    Ok(Configuration {
        image,
        mountpoint,
        engine,
        environment,
    })
}

fn engine_for_profile(value: &Value, profile: &str) -> String {
    let defaults = &value["defaults"]["engine"];
    let profile = &value["profiles"][&profile]["engine"];
    match profile.is_string() {
        true => profile.as_str().unwrap_or("").trim().to_string(),
        false => match defaults.is_string() {
            true => defaults.as_str().unwrap_or("").trim().to_string(),
            false => String::new(),
        },
    }
}

fn environment_for_profile(value: &Value, profile: &str) -> Vec<(String, String)> {
    // first check the profile to see if environment is defined, note that
    // an empty object here will cause an override of the defaults, this might
    // be seen as a feature, not sure yet.
    let profile = &value["profiles"][&profile]["environment"];
    if let Some(environment) = environment_from_profile_value(&profile) {
        return environment;
    }

    // find the default environment, if it exists
    let defaults = &value["defaults"]["environment"];
    if let Some(environment) = environment_from_profile_value(&defaults) {
        return environment;
    }

    // no environment found
    Vec::new()
}

fn environment_from_profile_value(profile: &Value) -> Option<Vec<(String, String)>> {
    if profile.is_mapping() {
        let mut environment = Vec::new();
        for (name, value) in profile.as_mapping().unwrap() {
            environment.push((
                String::from(name.as_str().unwrap()),
                value.as_str().unwrap_or("").trim().to_string(),
            ));
        }
        return Some(environment);
    }

    None
}

fn get_config_filename() -> Result<String> {
    let home_key = "HOME";
    let home = env::var(home_key)?;

    let possible_file_paths = [
        String::from("./.container-run.conf"),
        [home.as_str(), ".container-run.conf"].join("/"),
        [home.as_str(), ".config/container-run/container-run.conf"].join("/"),
    ];

    for p in possible_file_paths {
        let f = File::open(&p);
        match f {
            Ok(_) => {
                debug!("found configuration file {:?}", p);
                return Ok(p);
            }
            _ => debug!("configuration file {:?}, not found", p),
        }
    }

    bail!("no configuration file found");
}

fn profile_for_basename(value: &Value, basename: &str) -> String {
    let profile = &value["basenames"][basename]["profile"];
    match profile.is_string() {
        true => String::from(profile.as_str().unwrap_or("defaults")),
        _ => String::from("defaults"),
    }
}

fn image_for_profile(value: &Value, profile: &str) -> Result<String> {
    let default_image = &value["defaults"]["image"];
    let image = &value["profiles"][profile]["image"];
    match image.is_string() {
        true => match image.as_str() {
            Some(i) => Ok(i.to_string()),
            None => bail!("unable to read image value from config"),
        },
        _ => match default_image.is_string() {
            true => match default_image.as_str() {
                Some(i) => Ok(i.to_string()),
                None => bail!("unable to read default image value from config"),
            },
            _ => bail!("no image found for profile {:?}, or default", profile),
        },
    }
}

fn mountpoint_for_profile(value: &Value, basename: &str, profile: &str) -> Result<String> {
    let default_mountpoint = &value["defaults"]["mountpoint"];
    let mountpoint = &value["profiles"][profile]["mountpoint"];
    match mountpoint.is_string() {
        true => match mountpoint.as_str() {
            Some(m) => Ok(m.to_string().replace("{basename}", basename)),
            None => bail!("unable to read mountpoint from config"),
        },
        _ => match default_mountpoint.is_string() {
            true => match default_mountpoint.as_str() {
                Some(m) => Ok(m.to_string().replace("{basename}", basename)),
                None => bail!("unable to read default mountpoint from config"),
            },
            _ => bail!("no mountpoint found for profile {:?}, or default", profile),
        },
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn create_valid_configdata() -> Value {
        let yaml = "
        basenames:
            testproject:
                profile: golang-github
        defaults:
            engine: podman
            image: docker.io/library/bash
            mountpoint: /src/{basename}
            environment:
                DEFAULT: YES
        profiles:
            golang-github:
                engine: docker
                image: docker.io/library/golang:latest
                mountpoint: /go/src/github.com/{basename}
            with-env-0:
                engine: docker
                image: docker.io/library/golang:latest
                mountpoint: /go/src/github.com/{basename}
                environment: {}
            with-env-1:
                engine: docker
                image: docker.io/library/golang:latest
                mountpoint: /go/src/github.com/{basename}
                environment:
                    HOST_VAR: \"\"
            with-env-2:
                engine: docker
                image: docker.io/library/golang:latest
                mountpoint: /go/src/github.com/{basename}
                environment:
                    FOO: BAR
        ";
        serde_yaml::from_str(yaml).unwrap()
    }

    #[test]
    fn test_engine_for_profile_found() {
        let configdata = create_valid_configdata();
        let expected = "docker";
        let observed = engine_for_profile(&configdata, "golang-github");
        assert_eq!(observed, expected);
    }

    #[test]
    fn test_engine_for_profile_not_found() {
        let configdata = create_valid_configdata();
        let expected = "podman";
        let observed = engine_for_profile(&configdata, "noprofile");
        assert_eq!(observed, expected);
    }

    #[test]
    fn test_environment_for_profile_defaults() {
        let configdata = create_valid_configdata();
        let observed = environment_for_profile(&configdata, "golang-github");
        assert_eq!(observed.len(), 1);
        let expected = "DEFAULT";
        assert_eq!(observed[0].0, expected);
        let expected = "YES";
        assert_eq!(observed[0].1, expected);
    }

    #[test]
    fn test_environment_for_profile_empty() {
        let configdata = create_valid_configdata();
        let observed = environment_for_profile(&configdata, "with-env-0");
        assert!(observed.is_empty());
    }

    #[test]
    fn test_environment_for_profile_found_in_host_env() {
        let configdata = create_valid_configdata();
        let observed = environment_for_profile(&configdata, "with-env-1");
        assert_eq!(observed.len(), 1);
        let expected = "HOST_VAR";
        assert_eq!(observed[0].0, expected);
        assert!(observed[0].1.is_empty());
    }

    #[test]
    fn test_environment_for_profile_found() {
        let configdata = create_valid_configdata();
        let observed = environment_for_profile(&configdata, "with-env-2");
        assert_eq!(observed[0].0, String::from("FOO"));
        assert_eq!(observed[0].1, String::from("BAR"));
    }

    #[test]
    fn test_image_for_profile_found() {
        let configdata = create_valid_configdata();
        let expected = "docker.io/library/golang:latest";
        let observed = image_for_profile(&configdata, "golang-github").unwrap();
        assert_eq!(observed, expected);
    }

    #[test]
    fn test_image_for_profile_not_found() {
        let configdata = create_valid_configdata();
        let expected = "docker.io/library/bash";
        let observed = image_for_profile(&configdata, "noprofile").unwrap();
        assert_eq!(observed, expected);
    }

    #[test]
    fn test_mountpoint_for_profile_found() {
        let configdata = create_valid_configdata();
        let expected = "/go/src/github.com/noproject";
        let observed = mountpoint_for_profile(&configdata, "noproject", "golang-github").unwrap();
        assert_eq!(observed, expected);
    }

    #[test]
    fn test_mountpoint_for_profile_not_found() {
        let configdata = create_valid_configdata();
        let expected = "/src/noproject";
        let observed = mountpoint_for_profile(&configdata, "noproject", "nonexistent").unwrap();
        assert_eq!(observed, expected);
    }

    #[test]
    fn test_profile_for_basename_found() {
        let configdata = create_valid_configdata();
        let expected = "golang-github";
        let observed = profile_for_basename(&configdata, "testproject");
        assert_eq!(observed, expected);
    }

    #[test]
    fn test_profile_for_basename_not_found() {
        let configdata = create_valid_configdata();
        let expected = "defaults";
        let observed = profile_for_basename(&configdata, "noproject");
        assert_eq!(observed, expected);
    }
}
