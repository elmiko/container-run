# container-run.conf

The configuration file for container-run contains mappings of directory
basenames to run environment profiles. These profiles dictate the container
runtime and image to use, and the working directory mount path.

## Location

container-run will look in three places to find a configuration file, in
order of precedence they are:
* `./.container-run.conf`
* `~/.container-run.conf`
* `~/.config/container-run/container-run.conf`

## Format

The configuration file is [JSON](https://www.json.org/json-en.html) or
[YAML](https://yaml.org/) formatted.  The following examples demonstrates the schema:

```json
{
    "basenames": {
        "myproject": {"profile": "golang-github"}
    },
    "defaults": {
        "engine": "podman",
        "image": "docker.io/library/bash",
        "mountpoint": "/src/{basename}",
        "environment": {
            "VAR_FROM_HOST": ""
        }
    },
    "profiles": {
        "golang-github": {
            "engine": "docker",
            "image": "docker.io/library/golang:1.16",
            "mountpoint": "/go/src/github.com/{basename}",
            "environment": {
                "VAR_FROM_CONFIG": "somevalue"
            }
        }
    }
}
```

or

```yaml
basenames:
    myproject:
        profile: golang-github
defaults:
    engine: podman
    image: docker.io/library/bash
    mountpoint: /src/{basename}
    environment:
        VAR_FROM_HOST: ""
profiles:
    golang-github:
        engine: docker
        image: docker.io/library/golang:1.16
        mountpoint: /go/src/github.com/{basename}
        environment:
            VAR_FROM_CONFIG: somevalue
```

In this example, when running `container-run` in any directory named `myproject`
the container runtime used will be `docker`, the image used will be
`docker.io/library/golang:1.16` and the working directory will be mounted at
`/go/src/github.com/myproject`. Note that container-run will substitute the
basename for `{basename}` in `mountpoint` fields.

The `defaults` field can be used to define a default engine, image, and mount
point to be used when the working directory base name is not explicitly
configured.

Environment variables can be passed into the container using the `environment`
field of the defaults for the named profile. Setting the value in a named
profile will override all defaults. Likewise an empty object (`{}`) for the
`environment` in a profile will cause the defaults not to be set in the
resultant container. Leaving an empty string (`""`) for the value of an
environment variable will cause it to be injected from the calling shell, unless
it is not set.
